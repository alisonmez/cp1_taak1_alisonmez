/*
 * Ali Sönmez
 * ali.sonmez@student.ehb.be
 * Multec 2017-2018
 * 1BA (PDT)

 import processing.pdf.*;
 ,PDF, "Taak1_AliSönmez.pdf"
 ....
 */

void setup() {
  size(600, 800);
  background(80);
  achtergrondBeeld();
  beeld1();
}

void achtergrondBeeld() {
  for (int i = 0; i <= 800; i+=10) {    //Iteratie waarbij startpunt i gelijk is aan 0 en eindpunt gelijk is aan 900 waarbij i constant stijgt met 10                      
    for(int x = 0; x <= 800; x+=10){
    line(0 + i, 0 + x, 600, width);                 //De lijnen die getrokken moeten worden van met hun resp. x en y coordinaten waarbij i met 10px de startpositie verhoogd of verlaagd
    line(600 - i, 800 - x, 0, width);
    }
  }
}

void beeld1() {
  for (int i=1; i<10; i+=2) {
    beeld(600, 800, i*25, 120, 8);
  }
}

int t = 0;             //hoek
int dotSize = 3;       //punt grootte

void beeld(float strGr, float strKl, float distanceStr, int dot, int loop) {

  float R = strGr;        //straal grootte cirkel
  float r = strKl;        //straal kleine cirkel
  float a = distanceStr;    //afstand tot midden r
  int dots = dot;         //aantal punten
  int loops = loop;

  colorMode(HSB, 100);
  for (float i = 0; i<TWO_PI*(R/r)*loops; i+=TWO_PI/dots) {
    float alpha = (r/R)*i;                      //hoek in R
    float beta = (1-(r/R))*i+t;                   //hoek in r
    float x1 = width/2+cos(alpha)*((R-r)/6);    //x positie van r
    float y1 = width/1.5+sin(alpha)*((R-r)/6);    //y positie van r
    float x2 = x1+cos(beta)*(a);                //x positie van a
    float y2 = y1-sin(beta)*(a);                //y positie van a
    noStroke();
    fill(40/(TWO_PI*(R/r)*loops)*i, 100, 100, 30);
    ellipse(x2, y2, dotSize*3, dotSize*3);
    fill(10);
    ellipse(x2, y2, dotSize*2, dotSize*2);
  }
}